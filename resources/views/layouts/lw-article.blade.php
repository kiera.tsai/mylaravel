<div>
    <!-- Page Heading -->
    <header class="bg-white shadow">
        <div class="max-w-7xl mx-auto py-6 px-4 sm:px-6 lg:px-8">
            <div class="flex field">
                <div class="nowrap font-semibold text-xl text-gray-800 py-2">
                    Livewire -
                    <a href="{{ route('lw.articles') }}" class="hover:text-gray-400 dark:hover:text-white">文章分享</a>
                    @stack('function_path')
                </div>
                <div class="w-full text-center">
                    @if( session()->has('notice') )
                    <label class="font-semibold text-red-500 text-lg text-center bg-gray-200 px-6 py-2 rounded">
                            訊息：{{ session()->pull('notice') }}
                    </label>
                    @endif
                </div>
                <div class="text-right nowrap">
                    @auth
                    <a href="{{ route('lw.articles.admin') }}" class="ml-2 px-4 py-2 bg-gray-800 border border-transparent rounded text-white hover:bg-gray-700 focus:bg-gray-700 active:bg-gray-900">我的文章</a>
                    <a href="{{ route('lw.articles.create') }}" class="ml-2 px-4 py-2 bg-gray-800 border border-transparent rounded text-white hover:bg-gray-700 focus:bg-gray-700 active:bg-gray-900">新增文章</a>
                    @endauth
                </div>
            </div>
            <div>
                ( 範例登入帳號：guest@mysite.com、登入密碼：guesspwd )
            </div>
        </div>
    </header>

    <!-- Page Content -->
    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8 mb-6">
            <div class="bg-white overflow-hidden shadow-xl sm:rounded-lg">
                @stack('main')
            </div>
        </div>
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8 mb-6">
            @stack('sub_main')
        </div>
    </div>
</div>