@extends('layouts.article')

@php
    $admin_flag = preg_match("/(\/articles\/admin(\/)?$)/i",session()->get('redirect_url')) ? true : false;
@endphp

@push('function_path')
    @if($admin_flag)
        &gt; <a href="{{ route('articles.admin') }}" class="hover:text-gray-400 dark:hover:text-white">我的文章</a>
    @endif
@endpush

@push('main')
<div class="mx-6 mb-3">
    <div class="grid-cols-2 mt-4">
        <div class="text-2xl font-semibold text-indigo-600">
            {{ $article->title }}
        </div>
        <div class="text-right">
        @auth
            @if ( auth()->user()->id == $article->user_id )
                <a href="{{ route('articles.edit', $article) }}" class="mr-1 text-sm px-4 py-2 bg-gray-500 border border-transparent rounded-md text-white hover:bg-gray-700 focus:bg-gray-700 active:bg-gray-900">編輯</a>
            @endif
        @endauth
                <button type="button" class="mr-1 text-sm px-4 py-2 bg-gray-500 border border-transparent rounded-md text-white hover:bg-gray-700 focus:bg-gray-700 active:bg-gray-900" onclick="history.back()">回上一頁</button>
        </div>
    </div>
    <div class="mt-3 inline-flex">
        <div class="text-green-600 mx-2">{{ $article->user->name }}</div>  - 
        <div class="text-gray-500 mx-2">{{ $article->updated_at }} 最後更新</div> 
    </div>
    <hr>
    <div class="mt-3 mx-2 text-lg">
        {!! html_entity_decode(nl2br($article->content)) !!}
    </div>
    @auth
        <div class="mt-3 bg-gray-200 py-3 rounded">
            <form action="{{ route('articles.store') }}" method="post">
                @csrf
                <input type="hidden" name="parent_id" value="{{ $article->id }}">
                <div class="mx-2 text-lg inline-flex">
                    <div class="text-lg inline-flex">
                        <div class="mr-2 text-green-600">{{ Auth::user()->name }}</div> 留言：
                        <textarea name="content" cols="60" rows="5" class="border border-gray-300 p-2 rounded-md">{{ old('content') }}</textarea>
                    </div>
                    <div class="ml-2 relative w-60">
                        <div class="text-sm text-red-600 mt-2 ml-2">
                            @error('content')
                                {{ $message }}
                            @enderror
                        </div>
                        <div class="pb-0">
                            <button type="submit" class="text-sm px-4 py-2 bg-gray-500 rounded-md text-white hover:bg-gray-700 focus:bg-gray-700 active:bg-gray-900">送出</button>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    @endauth
</div>
@endpush

@push('sub_main')
    <div class="bg-white overflow-hidden shadow-xl sm:rounded-lg">
    @foreach($comment_articles as $article)
        <div class="mx-6 mt-3 inline-flex w-full">
            <div><img src="/images/comment.png"></div>
            <div class="mx-2 w-eighty">
                <div class="inline-flex">
                    <div class="text-green-600">{{ $article->user->name }}</div>  - 
                    <div class="text-gray-500 mx-2">{{ $article->created_at }} 留言</div> 
                </div>
                <div class="mt-2 px-2 py-2 text-lg bg-gray-200 rounded w-full">
                    <pre>{{ $article->content }}</pre>
                </div>
            </div>

            @auth
            @if ( auth()->user()->id == $article->user_id )
            <div class="relative w-20">
                <div class="pb-0">
                    <form action="{{ route('articles.destroy', $article) }}" method="post" class="inline-flex">
                        @csrf
                        @method('DELETE')
                        <input type="hidden" name="comment" value="1">
                        <button type="submit" class="mr-1 text-sm px-4 py-2 bg-gray-500 border rounded-md text-white hover:bg-gray-700 focus:bg-gray-700 active:bg-gray-900">刪除</button>
                    </form>
                </div>
            </div>
            @endif
            @endauth        


        </div>
    @endforeach
    </div>
@endpush