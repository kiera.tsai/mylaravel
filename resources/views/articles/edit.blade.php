@extends('layouts.article')

@push('function_path')
    &gt; 編輯文章
@endpush

@push('main')
<div class="flex justify-center py-4">
    <form action="{{ route('articles.update', $article) }}" method="post">
        @csrf
        @method('PUT')
        <div class="flex field">
            <div class="mt-2 mr-2 font-semibold nowrap">標題</div>
            <div class="w-full">
                <input type="text" value="{{ old('title', $article->title) }}" name="title" class="border border-gray-300 p-2 w-full">
                <div class="text-sm text-red-600 mt-2 ml-2">
                @error('title')
                    {{ $message }}
                @enderror
                </div>
            </div>
        </div>
        <div class="flex field mt-4">
            <div class="mt-2 mr-2 font-semibold nowrap">內文</div>
            <div class="w-full">
                <textarea name="content" cols="60" rows="12" class="border border-gray-300 p-2">{{ old('content', $article->content) }}</textarea>
                <div class="text-sm text-red-600 mt-2 ml-2">
                @error('content')
                    {{ $message }}
                @enderror
                </div>
            </div>
        </div>
        <div class="flex field mt-4">
            <div class="mr-2 font-semibold">狀態</div>
            <div>
            <input type="radio" value="draft" name="state" @if( old('state', $article->state) == 'draft' ) checked @endif> 草稿
                <input type="radio" value="private" name="state" @if( old('state', $article->state) == 'private' ) checked @endif> 不公開
                <input type="radio" value="published" name="state" @if( old('state', $article->state) == 'published' ) checked @endif> 發佈
            </div>
        </div>

        <div class="flex justify-center py-4 mt-6">
            <button type="button" class="mr-1 text-sm px-4 py-2 bg-gray-500 border border-transparent rounded-md text-white uppercase tracking-widest hover:bg-gray-700 focus:bg-gray-700 active:bg-gray-900 focus:outline-none focus:ring-2 focus:ring-indigo-500 focus:ring-offset-2" onclick="history.back()">回上一頁</button>
            <button type="submit" class="mr-1 text-sm px-4 py-2 bg-gray-500 border border-transparent rounded-md text-white uppercase tracking-widest hover:bg-gray-700 focus:bg-gray-700 active:bg-gray-900 focus:outline-none focus:ring-2 focus:ring-indigo-500 focus:ring-offset-2">確定</button>
        </div>
    </form>
</div>
@endpush