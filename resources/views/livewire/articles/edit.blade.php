@extends('layouts.lw-article')

@push('function_path')
    &gt; 編輯文章
@endpush

@push('main')
    @livewire('articles.form', ['article_id' => $article_id])
@endpush
