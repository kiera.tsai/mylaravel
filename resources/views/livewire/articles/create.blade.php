@extends('layouts.lw-article')

@push('function_path')
    &gt; 新增文章
@endpush

@push('main')
    <livewire:articles.form></livewire:articles.form>
@endpush
