@extends('layouts.lw-article')

@push('function_path')
    @if($is_admin)
        &gt; <a href="{{ route('lw.articles.admin') }}" class="hover:text-gray-400 dark:hover:text-white">我的文章</a>
    @endif
@endpush

@push('main')
    @foreach($articles as $article)
    <div wire:key="article{{ $article->id }}">
        <p class="ml-3 mt-4 text-2xl font-semibold text-indigo-600 hover:text-indigo-400 dark:hover:text-white">
            <a href="{{ route('lw.articles.show', $article) }}">{{ $article->title }}</a>
        </p>
        <div class="border-b border-gray-200">
            <div class="grid-cols-2">
                <div class="ml-3 mt-3 flex">
                    {{ $article->updated_at->format('Y-m-d') }} ‧ 
                    由 <div class="text-green-600 mx-2">{{ $article->user->name }}</div> 分享
                    @if ($is_admin)
                        <img src="/images/{{ $article->state }}.png" class="ml-4">
                    @endif
                    <div class="mx-2">
                        留言( {{ $cnt_arr[$article->id]['count'] ?? 0 }} )
                    </div>
                </div>

                @auth
                @if ( auth()->user()->id == $article->user_id )
                <div class="text-right mb-2">
                    <a href="{{ route('lw.articles.edit', $article) }}" class="mr-1 text-sm px-4 py-2 bg-gray-500 border rounded-md text-white hover:bg-gray-700 focus:bg-gray-700 active:bg-gray-900">編輯</a>
                    <button type="button" wire:click="$emit('article_destroy', {{ $article->id}} )" onclick="confirm('是否確定刪除 ?') || event.stopImmediatePropagation()" class="mr-1 text-sm px-4 py-2 bg-gray-500 border rounded-md text-white hover:bg-gray-700 focus:bg-gray-700 active:bg-gray-900">刪除</button>
                </div>
                @endif
                @endauth        
            </div>
        </div>
    </div>
    @endforeach

    <div class="mt-8 ml-3 mr-3">{{ $articles->links() }}</div>
@endpush