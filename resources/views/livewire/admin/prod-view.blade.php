@extends('layouts.prod')

@push('function_path')
    &gt; 商品頁
@endpush

@push('main')
    @livewire('prod-content', ['prod_id' => $prod_id])
@endpush
