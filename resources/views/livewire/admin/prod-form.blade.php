<div class="ml-4 mr-4 py-4 md:grid md:grid-cols-1 justify-center">
    <div class="flex">
        <div>
            <div class="flex field">
                <label class="w-20 mt-2 mr-2 font-semibold item_subtitle text-center">商品名稱</label>
                <div class="w-full">
                    <input type="text" wire:model.defer="name" class="border border-gray-300 p-2 w-3/4">
                    <span class="text-sm text-red-600 mt-2 ml-2">(必填)
                    @error('name')
                        <div>
                            {{ $message }}
                        </div>
                    @enderror
                    </span>
                </div>
            </div>
            <div class="flex field mt-2">
                <label class="w-20 mr-2 font-semibold item_subtitle text-center">商品圖片</label>
                <div class="w-full">
                    <input type="file" wire:model="upload_image" accept="image/png, image/jpg, image/jpeg" />
                    <span class="text-sm text-red-600 mt-2 ml-2">(必選 / 請勿超過 1MB)</span>
                    <div>
                        <span class="text-sm text-red-600">
                        @error('upload_image')
                            <div>
                                {{ $message }}
                            </div>
                        @enderror
                        </span>
                    </div>
                </div>
            </div>
            <div class="flex field mt-2">
                <label class="w-20 mt-2 mr-2 font-semibold item_subtitle text-center">定價</label>
                <div class="w-full">
                    <input type="number" wire:model.defer="price" ondrop="return false;" class="border border-gray-300 p-2">
                    <span class="text-sm text-red-600 mt-2 ml-2">(必填)
                    @error('price')
                        <div>
                            {{ $message }}
                        </div>
                    @enderror
                    </span>
                </div>
            </div>
            <div class="flex field mt-2">
                <label class="w-20 mt-2 mr-2 font-semibold item_subtitle text-center">促銷價</label>
                <div class="w-full">
                    <input type="number" wire:model.defer="sale_price" ondrop="return false;" class="border border-gray-300 p-2">
                    <span class="text-sm text-red-600 mt-2 ml-2">
                    @error('sale_price')
                        <div>
                            {{ $message }}
                        </div>
                    @enderror
                    </span>
                </div>
            </div>
            <div class="flex field mt-2">
                <label class="w-20 mt-2 mr-2 font-semibold item_subtitle text-center">商品數量</label>
                <div class="w-full">
                    <input type="number" wire:model.defer="amount" ondrop="return false;" onpaste="return false;" class="border border-gray-300 p-2">
                    <span class="text-sm text-red-600 mt-2 ml-2">(必填)
                    @error('amount')
                        <div>
                            {{ $message }}
                        </div>
                    @enderror
                    </span>
                </div>
            </div>
            <div class="flex field mt-2">
                <label class="w-20 mr-2 font-semibold item_subtitle text-center">商品狀態</label>
                <div class="w-full">
                    <input type="radio" wire:model.debounce.1ms="state" value="online"> 上架
                    <input type="radio" wire:model.debounce.1ms="state" value="offline"> 不上架
                </div>
            </div>
            <div class="flex field mt-2">
            <label class="w-20 mr-2 font-semibold item_subtitle text-center">商品類別</label>
                <div class="w-full">
                    @foreach($categories as $k => $cate)
                        @if (($k > 0) && ($k % 8 == 0))
                            <br>
                        @endif
                        <input type="checkbox" wire:model.defer="prod_categories" value="{{ $cate->id }}" wire:key="cate{{ $cate->id }}">
                        <label class="mr-2">{{ $cate->name }}</label>
                    @endforeach
                    <span class="text-sm text-red-600 mt-2 ml-2">(必選)
                    @error('prod_categories')
                        <div>
                            {{ $message }}
                        </div>
                    @enderror
                    </span>
                </div>
            </div>
            <div class="flex field mt-2">
                <label class="w-20 mt-2 mr-2 font-semibold item_subtitle text-center">廣告標語</label>
                <div class="w-full">
                    <textarea wire:model.defer="slogan" cols="60" rows="5" class="border border-gray-300 p-2">{{ old('content') }}</textarea>
                    <span class="text-sm text-red-600 mt-2 ml-2">
                    @error('slogan')
                        <div>
                            {{ $message }}
                        </div>
                    @enderror
                    </span>
                </div>
            </div>
        </div>
        <div>
            @if ($upload_image)
                <img src="{{ $upload_image->temporaryUrl() }}" class="border border-gray-800">
            @elseif (is_numeric($prod_id))
                <img src="{{ asset('storage/prods/'.$prod_id.'.'.$image_ext) }}" class="border border-gray-800">
            @endif
        </div>
    </div>
    <div>
       <div class="flex field mt-2">
            <label class="w-20 mt-2 mr-2 font-semibold item_subtitle text-center">商品介紹</label>
            <div class="w-full">
                <textarea wire:model.defer="content" cols="60" rows="12" class="border border-gray-300 p-2">{{ old('content') }}</textarea>
                <span class="text-sm text-red-600 mt-2 ml-2">(必填)
                @error('content')
                    <div>
                        {{ $message }}
                    </div>
                @enderror
                </span>
            </div>
        </div>
        <div class="flex justify-center py-4 mt-6">
            <button type="button" class="mr-1 text-sm px-4 py-2 bg-gray-500 border border-transparent rounded-md text-white uppercase tracking-widest hover:bg-gray-700 focus:bg-gray-700 active:bg-gray-900 focus:outline-none focus:ring-2 focus:ring-indigo-500 focus:ring-offset-2" onclick="history.back()">回上一頁</button>
            <button type="button" wire:click="submit" class="mr-1 text-sm px-4 py-2 bg-gray-500 border border-transparent rounded-md text-white uppercase tracking-widest hover:bg-gray-700 focus:bg-gray-700 active:bg-gray-900 focus:outline-none focus:ring-2 focus:ring-indigo-500 focus:ring-offset-2">確定</button>
            @if(is_null($prod_id))
                <button type="button" wire:click="submit('prods.create')" class="mr-1 text-sm px-4 py-2 bg-gray-500 border border-transparent rounded-md text-white uppercase tracking-widest hover:bg-gray-700 focus:bg-gray-700 active:bg-gray-900 focus:outline-none focus:ring-2 focus:ring-indigo-500 focus:ring-offset-2">確定並繼續新增</button>
            @endif
        </div>
    </div>
</div>