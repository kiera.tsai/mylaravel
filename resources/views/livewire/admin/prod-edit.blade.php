@extends('layouts.prod')

@push('function_path')
    &gt; 編輯商品
@endpush

@push('main')
    @livewire('admin.prod-form', ['prod_id' => $prod_id])
@endpush
