@extends('layouts.prod')

@push('function_path')
    &gt; 新增商品
@endpush

@push('main')
    <livewire:admin.prod-form></livewire:admin.prod-form>
@endpush
