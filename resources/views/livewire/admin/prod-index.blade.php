@extends('layouts.prod')

@push('main')
    @if($DisplayState == 'VIEW')
        @livewire('prod-content', ['prod_id'=>$prod_id])
    @else
        @livewire('admin.prod-list', [
            'list_state'=>$list_state,
            'sort_key'=>$sort_key,
            'sort_order'=>$sort_order,
            'list_page'=>$list_page,
            'prod_categories'=>implode(',', $prod_categories),
        ])
    @endif
@endpush

@push('scripts')
<script>    
window.addEventListener('stateAlert', event => {
    alert( event.detail.msg );
})

window.addEventListener('destroyAlert', event => {
    alert( event.detail.msg );
})
</script>
@endpush