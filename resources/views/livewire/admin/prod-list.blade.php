<div>
    <div class="grid-cols-2">
        <div class="ml-2 py-4">
            <label class="font-semibold">商品狀態：</label>
            <input type="radio" wire:model="list_state" value="" class="mr-1" @if( !$list_state )  checked @endif>全部(未刪除)
            <input type="radio" wire:model="list_state" value="online" class="ml-3 mr-1" @if( $list_state == 'online')  checked @endif>已上架
            <input type="radio" wire:model="list_state" value="offline" class="ml-3 mr-1" @if( $list_state == 'offline' ) checked @endif>已下架
            <input type="radio" wire:model="list_state" value="delete" class="ml-3 mr-1" @if( $list_state == 'delete' ) checked @endif>已刪除
        </div>
        <div class="text-right mr-2 mt-8">
            共 <label class="text-red-600">{{ $prods->total() }}</label> 筆
        </div>
    </div>
    <div>
        <div class="ml-2 py-1">
            <label class="font-semibold">商品類別：</label>
            @foreach($categories as $k => $cate)
                <input type="checkbox" wire:model="prod_categories" value="{{ $cate->id }}" wire:key="cate{{ $cate->id }}">
                <label class="mr-2">{{ $cate->name }}</label>
            @endforeach
        </div>
    </div>
    <div class="border-b border-gray-400 py-2">
        <div class="bg-gray-300 py-2 ml-2 mr-2 flex">
            <div class="w-10 text-center font-semibold text-sm">上架</div>
            <div class="w-10 text-center font-semibold text-sm border-r border-gray-500 mr-1">下架</div>
            <div class="w-eighty flex text-center justify-between font-semibold">
                <div class="w-thirty">商品名稱</div>
                <div class="w-sixty">商品介紹</div>
                <div class="w-ten flex px-2">
                    <div class="mr-1 mt-1">
                        <a href="#" onclick="Livewire.emit('changeSort', 'real_price', 'desc');"><img width="16px" src="{{ asset('images/arrow-down.png') }}"></a>
                    </div>
                    <label class="nowrap">價格</label>
                    <div class="ml-1 mt-1">
                        <a href="#" onclick="Livewire.emit('changeSort', 'real_price', 'asc');"><img width="16px" src="{{ asset('images/arrow-up.png') }}"></a>
                    </div>
                </div>
            </div>
            <div class="w-10">&nbsp;</div>
        </div>
    </div>

    @if ($prods->total() > 0)
        @foreach($prods as $prod)
        <div class="border-b border-gray-400 py-2" wire:key="prod-div-{{ $prod->id }}">
            <div class="ml-2 mr-2 flex field">
                <div class="w-10 text-center">
                    @if( is_null($prod->deleted_at) )
                        <input type="radio" wire:click="$emit('state_update', {{ $prod->id }}, 'online')" name="state{{ $prod->id }}" value="online" class="mt-4" @if( is_null($prod->deleted_at) && ($prod->state == 'online') ) checked @endif>
                    @endif
                </div>
                <div class="w-10 text-center border-r border-gray-400">
                    @if( is_null($prod->deleted_at) )
                        <input type="radio" wire:click="$emit('state_update', {{ $prod->id }}, 'offline')" name="state{{ $prod->id }}" value="offline" class="mt-4 mr-1" @if( is_null($prod->deleted_at) && ($prod->state == 'offline') ) checked @endif>
                    @endif
                </div>
                <img src="{{ asset('storage/prods/small/'.$prod->id.'.'.$prod->image_ext) }}" class="ml-2 border border-gray-800">
                <div class="w-3/4">
                    <a href="#" onclick="Livewire.emit('prod_view', {{ $prod->id }});" class="flex justify-between flex font-semibold text-gray-500 hover:text-gray-900 dark:hover:text-white">
                        <div class="w-thirty ml-2 mr-2 border-r border-gray-400">{{ $prod->name }}</div>
                        <div class="w-sixty ml-2 mr-2 border-r border-gray-400">{{ mb_substr($prod->content, 0, 70).'...' }}</div>
                        <div class="w-ten mt-3 ml-2 mr-2 text-right">
                            @if( !is_null($prod->sale_price) )
                                <s>${{ number_format($prod->price) }}</s><br>
                            @endif
                            <label class="text-red-600">${{ number_format($prod->real_price) }}</label>
                        </div>
                    </a>
                </div>
                <div class="w-36 text-right nowrap border-l border-gray-400">
                    @if( is_null($prod->deleted_at) )
                    <div class="mt-3">
                        <a href="{{ route('prods.edit', $prod) }}" class="text-sm px-4 py-2 bg-gray-500 border rounded-md text-white hover:bg-gray-700 focus:bg-gray-700 active:bg-gray-900">編輯</a>
                        <button type="button" wire:click="$emit('prod_destroy', {{ $prod->id}} )" onclick="confirm('是否確定刪除 ?') || event.stopImmediatePropagation()" class="text-sm px-4 py-2 bg-gray-500 border rounded-md text-white hover:bg-gray-700 focus:bg-gray-700 active:bg-gray-900">刪除</button>
                    </div>
                    @else
                    <div class="mt-2 ml-2 text-left">
                        刪除日期：<br>
                        {{ $prod->deleted_at->format('Y-m-d')}}
                    </div>
                    @endif
                </div>
            </div>
        </div>
        @endforeach
    @else
    <div class="border-b border-gray-400 py-2">
        <div class="ml-2 mr-2 text-center">無商品資料</div>
    </div>
    @endif

    <div class="mt-8 ml-2 mr-2">
        {{ $prods->appends([
            'list_state'=>$list_state,
            'sort_key'=>$sort_key,
            'sort_order'=>$sort_order,
            'prod_categories'=>$prod_categories,
        ])->links() }}
    </div>
</div>