<div class="text-2xl font-medium text-white bg-red-500">錯誤訊息</div>
<hr class="mb-4">

<div class="text-2xl text-red-600 mt-2 text-center">{{ $errmsg }}</div>

<div class="flex justify-center py-4 mt-6">
    <button type="button" class="mr-1 text-sm px-4 py-2 bg-gray-500 border border-transparent rounded-md text-white uppercase tracking-widest hover:bg-gray-700 focus:bg-gray-700 active:bg-gray-900 focus:outline-none focus:ring-2 focus:ring-indigo-500 focus:ring-offset-2" onclick="history.back()">回上一頁</button>
</div>
