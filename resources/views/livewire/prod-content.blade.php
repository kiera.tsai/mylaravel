<div class="px-4 py-4">
    <div class="flex">
        <div class="mr-2">
            <img width="450px" src="{{ asset('storage/prods/'.$prod->id.'.'.$prod->image_ext) }}" class="ml-3 border border-gray-800" >
        </div>
        <div class="w-1/2 ml-2 py-4">
            <p class="text-xl font-semibold">{{ $prod->name }}</p>
            <p class="text-red-500 font-semibold mt-3">
                {!! html_entity_decode(nl2br($prod->slogan)) !!}
            </p>
            <br><br>
            <div class="font-semibold text-lg bg-gray-200 px-6 py-2 rounded">
                <span class="text-sm text-gray-700">網路價</span>
                @if( is_null($prod->sale_price) )
                    <span class="text-2xl text-red-600 font-semibold">${{ number_format($prod->price) }}</span>
                @else
                    <span class="text-2xl text-red-600 font-semibold">${{ number_format($prod->sale_price) }}</span>
                    <span class="ml-4 text-gray-400 font-semibold"><s>${{ number_format($prod->price) }}</s></span>
                @endif
            </div>
            <br><br>
            <div class="px-2 text-gray-900 text-xl">
                {!! html_entity_decode(nl2br($prod->content)) !!}
            </div>
        </div>
    </div>
    <div class="text-center py-4 mt-6">
        <button type="button" wire:click="$emit('history_back')" class="mr-1 text-sm px-4 py-2 bg-gray-500 border border-transparent rounded-md text-white uppercase tracking-widest hover:bg-gray-700 focus:bg-gray-700 active:bg-gray-900 focus:outline-none focus:ring-2 focus:ring-indigo-500 focus:ring-offset-2">回上一頁</button>
    </div>
</div>