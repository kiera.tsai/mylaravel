<?php

namespace App\Rules;

use Closure;
use Illuminate\Contracts\Validation\ValidationRule;

class ProdSalePrice implements ValidationRule
{
    protected $price;

    public function __construct($price)
    {
        $this->price = $price;         
    }

    public function validate(string $attribute, mixed $value, Closure $fail): void
    {
        if ($value >= $this->price) {
            $fail(':attribute 應低於定價');
        }
    }

}
