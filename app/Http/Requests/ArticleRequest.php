<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ArticleRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array|string>
     */
    public function rules(): array
    {
        return [
            'title' => 'required',
            'content' => 'required',
            'state' => 'required'
        ];
    }

    public function messages(): array
    {
        return [
            'required' => ':attribute 不可空白。',
        ];
    }

    public function attributes(): array
    {
        return [
            'title' => '標題',
            'content' => '內文',
        ];
    }
}
