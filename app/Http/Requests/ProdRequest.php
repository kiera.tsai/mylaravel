<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use App\Rules\ProdSalePrice;

class ProdRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return false;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array|string>
     */
    public function rules($prod_id, $price): array
    {
        return [
            'name' => 'required|min:3',
            'price' => 'required|numeric|gt:0',
            'sale_price' => ['nullable', 'numeric','gt:0', new ProdSalePrice($price)],
            'amount' => 'required|numeric|gt:0',
            'content' => 'required|min:10',
            'upload_image' => $prod_id ? 'nullable' : 'required|image|mimes:jpeg,png,jpg|max:1024',
            'prod_categories' => 'array|required',
        ];
    }

    public function messages(): array
    {
        return [
            'required' => ':attribute 不可空白。',
            'numeric' => ':attribute 需為數值。',
            'gt' => ':attribute 需大於0。',
            'name.min' => ':attribute 至少3個字以上。',
            'sale_price.between' => ':attribute 不應大於定價',
            'content.min' => ':attribute 至少10個字以上。',
            'upload_image.required' => ':attribute 未選',
            'upload_image.image' => '選擇 png、jpg、jpeg 圖檔',
            'upload_image.mimes' => '選擇 png、jpg、jpeg 圖檔',
            'upload_image.max' => ':attribute 超過 1MB',
            'prod_categories.required' => '請選擇:attribute',
        ];
    }

    public function attributes(): array
    {
        return [
            'name' => '商品名稱',
            'price' => '定價',
            'sale_price' => '促銷價',
            'amount' => '商品數量',
            'content' => '商品介紹',
            'upload_image' => '商品圖片',
            'prod_categories' => '商品類別',
        ];
    }
}
