<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use App\Models\Article;
use App\Http\Requests\ArticleRequest;

class ArticlesController extends Controller
{
    public function __construct() {
        $this->middleware('auth')->except(['index', 'show']);
    }

    public function index(Request $request) {
        session(['redirect_url' => $request->path()]);

        $perpage_cnt = 10;

        if ($request->route()->getName() == 'articles.admin') {
            $articles = auth()->user()->articles();
        } else {
            $articles = Article::where('state', 'published');
        }
        $articles = $articles->whereNull('parent_id')->with('user')->orderBy('created_at', 'desc')->paginate($perpage_cnt);

        // 統計留言數量
        $cnt_arr = array();
        if (count( $articles->pluck('id') ) > 0) {
            $cnt_arr = Article::select('parent_id', DB::raw('COUNT(id) AS count'))
                ->whereIn('parent_id', $articles->pluck('id') )
                ->groupBy('parent_id')->get()->keyBy('parent_id')->toArray();
        }

        return view('articles.index', ['articles'=>$articles, 'cnt_arr'=>$cnt_arr]);
    }

    public function show($id) {
        if ( $article = Article::where('id', $id)->whereNull('parent_id')->first()) {
            if ( ($article->state == 'published') || 
                 (Auth::check() && ($article->user_id == Auth::user()->id)) ) {
                $comment_articles = Article::where('state', 'published')->where('parent_id', $id)->with('user')->get();

                return view('articles.show', ['article'=>$article, 'comment_articles'=>$comment_articles]);
            } else {
                return view('articles.autherr', ['errmsg'=>'未經授權處理']);
            }    
        } else {
            return view('articles.autherr', ['errmsg'=>'查無資料']);
        }
    }

    public function create() {
        return view('articles.create');
    }

    public function store(Request $request) {
        $r = new ArticleRequest;

        if (is_null($request->get('parent_id'))) {
            $data = $request->validate($r->rules(), $r->messages(), $r->attributes());
            auth()->user()->articles()->create($data);
    
            return $this->redirect_list('文章新增完成!');
        } else {
            // 留言文章
            $parent_id = $request->get('parent_id');

            $rules = ['content'=>'required','parent_id'=>'required'];
            $attributes = ['content'=>'留言內容'];
    
            $data = $request->validate($rules, $r->messages(), $attributes);
            $data['state'] = 'published';
            auth()->user()->articles()->create($data);

            return redirect()->route('articles.show', ['article' => $parent_id])->with('notice', '留言完成!');
        }
    }

    public function edit(Request $request, $id) {
        if ($article = auth()->user()->articles()->find($id)) {
            return view('articles.edit', ['article'=>$article]);
        } else {
            return view('articles.autherr', ['errmsg'=>'未經授權處理']);
        }
    }

    public function update(ArticleRequest $request, $id) {
        if ($article = auth()->user()->articles()->find($id)) {
            $article->update( $request->all() );

            return $this->redirect_list('文章更新完成!');
        } else {
            return view('articles.autherr', ['errmsg'=>'未經授權處理']);
        }
    }

    public function destroy(Request $request, $id) {
        if ($article = auth()->user()->articles()->find($id)) {
            $article->delete();

            if ($request->get('comment') == 1) {
                // 刪除留言
                $parent_id = $article->parent_id;
                return redirect()->route('articles.show', ['article' => $parent_id])->with('notice', '留言已刪除!');
            } else {
                return $this->redirect_list('文章已刪除!');
            }
        } else {
            return view('articles.autherr', ['errmsg'=>'未經授權處理']);
        }
    }

    public function redirect_list($notice) {
        if ( session()->has('redirect_url') ) {
            return redirect( session()->pull('redirect_url') )->with('notice', $notice);
        } else {
            return redirect()->route('articles')->with('notice', $notice);
        }
    }
}
