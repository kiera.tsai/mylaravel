<?php

namespace App\Http\Livewire\Admin;

use Livewire\Component;

class ProdCreate extends Component
{
    public function render()
    {
        return view('livewire.admin.prod-create');
    }
}
