<?php

namespace App\Http\Livewire\Admin;

use Livewire\Component;
use Livewire\WithFileUploads;
use Intervention\Image\Facades\Image;
use App\Http\Requests\ProdRequest;
use App\Models\Prod;
use App\Models\Category;

class ProdForm extends Component
{
    use WithFileUploads;

    // 宣告到時候 Input 會用到的欄位
    public $prod_id;
    public $name;
    public $price;
    public $sale_price;
    public $amount;
    public $state = 'online';
    public $slogan;
    public $content;
    public $image_ext;
    public $upload_image;
    public $prod_categories = [];

    protected $rules, $messages, $validationAttributes;

    public function mount($prod_id = null) {
        $this->prod_id = $prod_id;

        if ( $this->prod_id ) {
            if ( $prod = Prod::find($this->prod_id) ) {
                $this->prod_id = $prod->id;
                $this->name = $prod->name;
                $this->price = $prod->price;
                $this->sale_price = $prod->sale_price;
                $this->amount = $prod->amount;
                $this->state = $prod->state;
                $this->slogan = $prod->slogan;
                $this->content = $prod->content;
                $this->image_ext = $prod->image_ext;

                // 商品類別
                $this->prod_categories = $prod->categories->pluck('id')->toArray();
            }
        }
    }

    public function render()
    {
        if ( $this->prod_id && !$this->image_ext )
            return view('livewire.systemerr', ['errmsg'=>'查無此商品資料']);
        else {
            // 所有類別
            $categories = Category::orderBy('sort')->get();
            return view('livewire.admin.prod-form', ['categories'=>$categories]);
        }
    }

    public function submit($uri_name='prods') {
        // 商品資訊驗證
        $r = new ProdRequest;
        $this->rules = $r->rules($this->prod_id, $this->price);
        $this->messages = $r->messages();
        $this->validationAttributes  = $r->attributes();
        $this->validate();

        // 商品資料 (已驗證)
        $prod_data = [
            'name' => $this->name,
            'price' => $this->price,
            'sale_price' => is_numeric($this->sale_price) ? $this->sale_price : null,
            'real_price' =>is_numeric($this->sale_price) ? $this->sale_price : $this->price,
            'amount' => $this->amount,
            'state' => $this->state,
            'slogan' => $this->slogan,
            'content' => $this->content,
        ];
        if ( $this->upload_image )
            $prod_data['image_ext'] = $this->upload_image->getClientOriginalExtension();

        // DB: 商品資料
        $old_categories = [];
        if ( $this->prod_id ) {
            // 編輯商品
            $prod = Prod::find($this->prod_id);
            $prod->update($prod_data);
            $notice = '商品編輯完成!';

            //刪除商品類別
            $old_categories = $prod->categories->pluck('id')->toArray();
            $prod->categories()->detach( array_diff($old_categories, $this->prod_categories) );
        } else {
            // 建立商品
            $prod = Prod::create($prod_data);
            $this->prod_id = $prod->id;

            $notice = '商品新增完成!';
        }

        // DB: 商品類別
        $prod->categories()->attach( array_diff($this->prod_categories, $old_categories) );

        // 建立商品圖: 以商品ID為檔名
        if ( $this->upload_image ) {
            $image_name = $this->prod_id . '.' . $this->upload_image->getClientOriginalExtension();
            $image_path = storage_path('app/' . $this->upload_image->storeAs('public/prods', $image_name));

            // 建立商品圖: medium (固定寬 150px，高度等比例縮放)
            $re_image_path = str_replace('/prods/', '/prods/medium/', $image_path);
            $this->resize_image($this->upload_image->getRealPath(), 150, $re_image_path);

            // 建立商品圖: small (固定寬 75px，高度等比例縮放)
            $re_image_path = str_replace('/prods/', '/prods/small/', $image_path);
            $this->resize_image($this->upload_image->getRealPath(), 75, $re_image_path);
        }

        return redirect()->route($uri_name)->with('notice', $notice);
    }

    // 建立商品圖:
    public function resize_image($src_image, $size, $image_path) {
        $re_image = Image::make($src_image);

        // 目錄是否存在
        if ( !realpath(dirname($image_path)) ) {
            mkdir(dirname($image_path), 0777, true);
        }

        // 固定寬 $size px，高度等比例縮放
        $re_image->resize($size, null, function ($constraint) {
            $constraint->aspectRatio();
        })->save($image_path);
    }
}
