<?php

namespace App\Http\Livewire\Admin;

use Livewire\Component;

class ProdEdit extends Component
{
    public $prod_id;

    public function mount($prod_id) {
        $this->prod_id = $prod_id;
    }

    public function render()
    {
        return view('livewire.admin.prod-edit');
    }
}
