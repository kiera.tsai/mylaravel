<?php

namespace App\Http\Livewire\Admin;

use Livewire\Component;
use Illuminate\Http\Request;

class ProdIndex extends Component
{
    public $DisplayState = 'LIST';

    public $prod_id;
    public $list_page;
    public $list_state;
    public $sort_key;
    public $sort_order;
    public $prod_categories = [];

    protected $listeners = ['set_pubvalue', 'prod_view', 'history_back'];
    protected $queryString = ['list_state', 'sort_key', 'sort_order', 'prod_categories'];

    public function mount(Request $r) {
        $this->list_page = $r->page;
    }

    public function render()
    {
        return view('livewire.admin.prod-index');
    }

    public function set_pubvalue($data) {
        foreach ($data as $k => $v) {
            if ( in_array($k, array('list_state', 'sort_key', 'sort_order', 'prod_categories')) ) {
                $this->{$k} = $v;
            }
        }
        $this->list_page = null;
    }

    // 顯示商品頁
    public function prod_view($prod_id) {
        $this->prod_id = $prod_id;
        $this->DisplayState = 'VIEW';
    }

    // 返回列表
    public function history_back() {
        $this->DisplayState = 'LIST';
    }
}
