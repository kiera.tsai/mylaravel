<?php

namespace App\Http\Livewire\Admin;

use Livewire\Component;
use Illuminate\Pagination\Paginator;
use App\Models\Prod;
use App\Models\Category;

class ProdList extends Component
{
    public $list_state;
    public $sort_key;
    public $sort_order;
    public $list_page;
    public $current_page;
    public $prod_categories = [];

    protected $listeners = ['changeSort', 'state_update', 'prod_destroy'];

    public function mount($list_state, $sort_key, $sort_order, $list_page, String $prod_categories) {
        $list_state = strtolower( $list_state );
        $sort_key = strtolower( $sort_key );
        $sort_order = strtolower( $sort_order );
        
        $this->list_state = in_array($list_state, ['online', 'offline', 'delete']) ? $list_state : '';
        $this->sort_key = in_array($sort_key, ['real_price']) ? $sort_key : null;
        $this->sort_order = in_array($sort_order, ['asc', 'desc']) ? $sort_order : null;
        $this->list_page = is_numeric($list_page) ? $list_page : null;
        $this->prod_categories = $prod_categories ? explode(',', $prod_categories) : [];
    }

    public function render()
    {
        $perpage_cnt = 10;

        $query = new Prod;
        switch ($this->list_state) {
            case 'online':
            case 'offline':
                $query =$query->where('state', $this->list_state);
                break;
            case 'delete':
                $query =$query->onlyTrashed();
                break;
        }

        // 商品類別
        if (count($this->prod_categories)) {
            $query = $query->whereHas('categories', function($q) {
                return $q->whereIn('category_id', $this->prod_categories);
            });
        }

        // 指定排序條件
        if ($this->sort_key && $this->sort_order)
            $query = $query->orderBy($this->sort_key, $this->sort_order);

        // 預設依新增降冪排序
        if ($this->list_page > 1) {
            Paginator::currentPageResolver(fn() => $this->list_page);
        }

        $query = $query->orderBy('created_at', 'desc')->paginate($perpage_cnt);
        $query->setPath('');
        // dd($query);
        $this->current_page = $query->currentPage();

        // 所有類別
        $categories = Category::orderBy('sort')->get();

        return view('livewire.admin.prod-list', ['prods'=>$query, 'categories'=>$categories]);
    }

    public function updatedListState() {
        $this->list_page = null;
        $this->emitUp('set_pubvalue', [
            'list_state' => $this->list_state,
        ]);
    }

    public function updatedProdCategories() {
        $this->list_page = null;
        $this->emitUp('set_pubvalue', [
            'prod_categories' => $this->prod_categories,
        ]);
    }

    public function changeSort($key, $order) {
        $this->sort_key = $key;
        $this->sort_order = $order;
        $this->list_page = null;

        $this->emitUp('set_pubvalue', [
            'sort_key' => $this->sort_key,
            'sort_order' => $this->sort_order,
        ]);        
    }

    public function state_update($prod_id, $state) {
        try {
            Prod::whereId($prod_id)->update([
                'state' => $state,
            ]);
            $msg = ($state == 'online') ? '商品狀態已變更: 上架' : '商品狀態已變更: 下架';
            $this->dispatchBrowserEvent( 'stateAlert', ['msg' => $msg] );
        } catch (\Exception $ex) {
            $this->dispatchBrowserEvent( 'stateAlert', ['msg' => '商品狀態更新失敗!'] );
        }
    }

    public function prod_destroy($prod_id) {
        if ($prod = Prod::findOrFail($prod_id)) {
            $prod->delete();
            $msg = '商品已刪除!';
        } else {
            $msg = '無法刪除: 查無此商品';
        }

        $this->dispatchBrowserEvent( 'destroyAlert', ['msg' => $msg]);
    }
}
