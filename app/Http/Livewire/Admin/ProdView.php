<?php

namespace App\Http\Livewire\Admin;

use Livewire\Component;

class ProdView extends Component
{
    public $prod_id;

    protected $listeners = ['history_back'];

    public function mount($prod_id) {
        $this->prod_id = $prod_id;
    }

    public function render()
    {
        return view('livewire.admin.prod-view');
    }

    // 回上一頁
    public function history_back() {
        redirect()->route('prods');
    }
}
