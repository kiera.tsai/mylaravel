<?php

namespace App\Http\Livewire;

use Livewire\Component;
use App\Models\Prod;

class ProdContent extends Component
{
    public $prod_id;

    public function mount($prod_id) {
        $this->prod_id = $prod_id;
    }

    public function render()
    {
        if ( $prod = Prod::withTrashed()->find($this->prod_id) )
            return view('livewire.prod-content', ['prod'=>$prod]);
        else
            return view('livewire.systemerr', ['errmsg'=>'查無此商品資料']);
    }
}