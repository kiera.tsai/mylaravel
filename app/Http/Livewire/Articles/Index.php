<?php

namespace App\Http\Livewire\Articles;

use Illuminate\Support\Facades\DB;
use Livewire\Component;
use Illuminate\Http\Request;
use App\Models\Article;

class Index extends Component
{
    public $is_admin = false;

    protected $listeners = ['article_destroy'];

    public function mount() {
        session( ['redirect_url' => url()->current()] );
    }

    public function render(Request $request)
    {
        $perpage_cnt = 10;

        if ( preg_match('/\/articles\/admin$/', session()->get('redirect_url')) ) {
            $this->is_admin = true;
            $articles = auth()->user()->articles();
        } else {
            $articles = Article::where('state', 'published');
        }
        $articles = $articles->whereNull('parent_id')->with('user')->orderBy('created_at', 'desc')->paginate($perpage_cnt);

        // 統計留言數量
        $cnt_arr = array();
        if (count( $articles->pluck('id') ) > 0) {
            $cnt_arr = Article::select('parent_id', DB::raw('COUNT(id) AS count'))
                ->whereIn('parent_id', $articles->pluck('id') )
                ->groupBy('parent_id')->get()->keyBy('parent_id')->toArray();
        }

        return view('livewire.articles.index', ['articles'=>$articles, 'cnt_arr'=>$cnt_arr]);

    }

    public function article_destroy($article_id) {
        if ($article = auth()->user()->articles()->find($article_id)) {
            $article->delete();

            session(['notice' => '文章已刪除!']);
        } else {
            session(['notice' => '刪除失敗，未經授權處理']);
        }        
        return true;
    }
}
