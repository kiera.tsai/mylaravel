<?php

namespace App\Http\Livewire\Articles;

use Livewire\Component;
use Illuminate\Support\Facades\Auth;
use App\Models\Article;
use App\Http\Requests\ArticleRequest;

class Show extends Component
{
    public $is_admin = false;
    public $article_id;
    public $parent_id;
    public $content;

    protected $listeners = ['common_destroy'];

    public function mount($id) {
        $this->article_id = $id;
        $this->parent_id = $id;
    }

    public function render()
    {
        $this->is_admin = preg_match('/\/articles\/admin$/', session()->get('redirect_url')) ? true : false;

        if ( $article = Article::where('id', $this->article_id)->whereNull('parent_id')->first()) {
            if ( ($article->state == 'published') || 
                 (Auth::check() && ($article->user_id == Auth::user()->id)) ) {
                $comments = Article::where('state', 'published')->where('parent_id', $this->article_id)->with('user')->get();

                return view('livewire.articles.show', ['article'=>$article, 'comments'=>$comments]);
            } else {
                return view('livewire.systemerr_extends', ['errmsg'=>'未經授權處理']);
            }  
        } else {
            return view('livewire.systemerr_extends', ['errmsg'=>'查無資料']);
        }
    }

    // 新增留言
    public function store() {
        $r = new ArticleRequest;

        $rules = ['content'=>'required','parent_id'=>'required'];
        $attributes = ['content'=>'留言內容'];

        $data = $this->validate($rules, $r->messages(), $attributes);
        $data['state'] = 'published';
        auth()->user()->articles()->create($data);

        $this->content = null;
        session(['notice' => '留言完成!']);

        return true;
    }

    // 刪除留言
    public function common_destroy($common_id) {
        if ($article = auth()->user()->articles()->find($common_id)) {
            $article->delete();

            session(['notice' => '留言已刪除!']);
        } else {
            session(['notice' => '刪除失敗，未經授權處理']);
        }

        return true;
    }
}
