<?php

namespace App\Http\Livewire\Articles;

use Livewire\Component;
use App\Models\Article;
use App\Http\Requests\ArticleRequest;

class Form extends Component
{
    public $article_id;
    public $title;
    public $content;
    public $state = 'published';

    public function mount($article_id = null) {
        $this->article_id = $article_id;

        if ( $this->article_id ) {
            if ( $article = Article::find($this->article_id) ) {
                $this->article_id = $article->id;
                $this->title = $article->title;
                $this->content = $article->content;
                $this->state = $article->state;
            }
        }
    }

    public function render()
    {
        if ( $this->article_id ) {
            // 編輯文章：權限檢查
            if (auth()->user()->articles()->find($this->article_id)) {
                return view('livewire.articles.form');
            } else {
                return view('livewire.systemerr', ['errmsg'=>'未經授權處理']);
            }
        } else {
            // 新增文章
            return view('livewire.articles.form');
        }
    }

    public function submit() {
        $r = new ArticleRequest;

        $data = $this->validate($r->rules(), $r->messages(), $r->attributes());

        if ( $this->article_id ) {
            // 編輯文章：權限檢查
            if ($article = auth()->user()->articles()->find( $this->article_id )) {
                $article->update( $data );
    
                return $this->redirect_list('文章更新完成!');
            } else {
                return $this->redirect_list('更新失敗，未經授權處理');
            }
        } else {
            // 新增文章
            auth()->user()->articles()->create($data);
            return $this->redirect_list('文章新增完成!');
        }
    }

    public function redirect_list($notice) {
        if ( session()->has('redirect_url') ) {
            return redirect( session()->pull('redirect_url') )->with('notice', $notice);
        } else {
            return redirect()->route('lw.articles')->with('notice', $notice);
        }
    }
}
