<?php

namespace App\Http\Livewire\Articles;

use Livewire\Component;

class Edit extends Component
{
    public $article_id;

    public function mount($id) {
        $this->article_id = $id;
    }

    public function render()
    {
        return view('livewire.articles.edit');
    }
}
