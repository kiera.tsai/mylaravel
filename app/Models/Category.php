<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    use HasFactory;

    protected $fillable = ['name', 'sort'];

    public function prods() {
        return $this->belongsToMany(Prod::class)
            ->withPivot('prod_id', 'category_id')
            ->withTimestamps();
    }
}
