<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Prod extends Model
{
    use HasFactory;
    use SoftDeletes;

    protected $fillable = [
        'name', 'price', 'sale_price', 'real_price', 'amount',
        'state', 'slogan', 'content', 'image_ext'
    ];

    public function categories() {
        // return $this->belongsToMany(Category::class)->withTimestamps();
        return $this->belongsToMany(Category::class, 'category_prod', 'prod_id', 'category_id')
            ->withPivot('prod_id', 'category_id')
            ->withTimestamps();
    }
}
