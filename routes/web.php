<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\ArticlesController;

Route::get('/', function () {
    return view('welcome');
})->name('root');

// 文章分享
Route::resource('/articles', ArticlesController::class);
Route::get('/articles/admin', [ArticlesController::class, 'index'])->name('articles.admin');
Route::get('/articles', [ArticlesController::class, 'index'])->name('articles');

// Livewire: Shopping
Route::get('/prods', App\Http\Livewire\Admin\ProdIndex::class)->name('prods');
Route::get('/prods/create', App\Http\Livewire\Admin\ProdCreate::class)->name('prods.create');
Route::get('/prods/{prod_id}', App\Http\Livewire\Admin\ProdView::class)->name('prods.view');
Route::get('/prods/{prod_id}/edit', App\Http\Livewire\Admin\ProdEdit::class)->name('prods.edit');

// Livewire: 文章分享
Route::get('/lw/articles', App\Http\Livewire\Articles\Index::class)->name('lw.articles');
Route::get('/lw/articles/admin', App\Http\Livewire\Articles\Index::class)->name('lw.articles.admin')->middleware('auth');
Route::get('/lw/articles/create', App\Http\Livewire\Articles\Create::class)->name('lw.articles.create')->middleware('auth');
Route::get('/lw/articles/{id}/edit', App\Http\Livewire\Articles\Edit::class)->name('lw.articles.edit')->middleware('auth');
Route::get('/lw/articles/{id}', App\Http\Livewire\Articles\Show::class)->name('lw.articles.show');

Route::middleware([
    'auth:sanctum',
    config('jetstream.auth_session'),
    'verified'
])->group(function () {
    Route::get('/dashboard', function () {
        return redirect()->route('articles');
        // return view('dashboard');
    })->name('dashboard');
});
