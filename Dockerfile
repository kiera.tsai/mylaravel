FROM php:8.2-cli

ENV \
  APP_DIR="/app" \
  APP_PORT="8000"

WORKDIR $APP_DIR
COPY . $APP_DIR

# Buile Storage public link
RUN cd $APP_DIR/public && ln -s ../storage/app/public storage

# Update packages
RUN apt-get update -y

# Install PHP and composer dependencies
RUN apt-get install -qq git curl libmcrypt-dev libjpeg-dev libpng-dev libfreetype6-dev libbz2-dev libzip-dev

# Clear out the local repository of retrieved package files
RUN apt-get clean

# Install needed extensions
# Here you can install any other extension that you need during the test and deployment process
RUN docker-php-ext-install pdo_mysql zip
RUN docker-php-ext-configure gd --enable-gd --with-freetype --with-jpeg
RUN docker-php-ext-install gd

# Install Composer
RUN curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer

# 更新 vendor
RUN cd $APP_DIR && composer require laravel/jetstream

# Install Node.Js & npm
RUN curl -fsSL https://deb.nodesource.com/setup_18.x | bash -
RUN apt-get install -y nodejs
RUN npm install
RUN npm run build

RUN php artisan env:decrypt --key=base64:bDYivPh1/R7RdHN2WQPAF2Djng4MMz7qjYIfJRkYvcU=
RUN php artisan key:generate
RUN php artisan migrate --force

CMD php artisan serve --host=0.0.0.0 --port=$APP_PORT