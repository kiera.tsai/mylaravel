<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::table('prods', function (Blueprint $table) {
            $table->integer('real_price')->unsigned()->comment('實際售價')->after('sale_price');
            $table->string('image_ext')->comment('圖片副檔名')->after('content');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::table('prods', function (Blueprint $table) {
            $table->dropColumn(['real_price', 'image_ext']);
        });
    }
};
