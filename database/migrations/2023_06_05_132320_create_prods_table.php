<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('prods', function (Blueprint $table) {
            $table->id()->unsigned();
            $table->string('name')->comment('商品名稱');
            $table->integer('price')->unsigned()->comment('商品定價');
            $table->integer('sale_price')->unsigned()->nullable()->comment('特價');
            $table->integer('amount')->unsigned()->comment('商品數量');
            $table->enum('state', ['online', 'offline'])->comment('商品狀態');
            $table->string('slogan')->nullable()->comment('廣告標語');
            $table->text('content')->comment('商品介紹');
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('prods');
    }
};
